#include <stdlib.h>
#include <stdio.h>

typedef struct Arvore Arvore;
struct Arvore
{
  int valor;
  Arvore *esq;
  Arvore *dir;
};

int is_binaria(Arvore *a, int min, int max);
int arv_bin_check(Arvore *a);

int is_binaria(Arvore *a, int min, int max)
{
  if (!a)
    return 1;
  if (a->valor < min || a->valor > max)
    return 0;
  return is_binaria(a->esq, min, a->valor) && is_binaria(a->dir, a->valor, max);
}

int arv_bin_check(Arvore *a)
{
  return is_binaria(a, 0, 5);
}
